let mensajes = ["prueba 1", "prueba 2", "prueba 3", "prueba 4", "prueba 5"];
const miDependencia = () => {
  const mesaje = mensajes[Math.floor(Math.random() * mensajes.length)];
  console.log(`\x1b[34m${mesaje}\x1b[89m`);
};

module.exports = {
  miDependencia,
};
